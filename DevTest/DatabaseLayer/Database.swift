//
//  Database.swift
//  DevTest


import UIKit
import RealmSwift

fileprivate let databaseQueue = DispatchQueue(label: "DatabaseQueue")


class Database: NSObject {
    
    func replaceAll<T>(by tArray: [T], callback: (()->())?) where T:Object {
        
        guard Thread.isMainThread else {
            fatalError("### Database: main thread required")
        }
        guard T.primaryKey() != nil  else {
            fatalError("### Database: primary key is required")
        }
        
        databaseQueue.async {
            autoreleasepool {
                do {
                    let realm = try Realm()
                    try realm.write() {
                        realm.delete(realm.objects(T.self))
                        tArray.forEach{ realm.add($0, update: false) }
                    }
                } catch let err {
                    print("### Database error: " + err.localizedDescription)
                }
                
                if let callback = callback {
                    DispatchQueue.main.async {
                        callback()
                    }
                }
            }
        }
    }
    
    func fetchAll<T>(_ ofType: T.Type, filter predicate: NSPredicate?, sort sortDescriptors: [SortDescriptor]?, callback: @escaping ([T])->()) where T:Object {
        
        databaseQueue.async {
            autoreleasepool {
                
                guard let realm = try? Realm() else {
                    fatalError("### Database: realm error")
                }
                
                var objects = realm.objects(ofType)
                
                if let predicate = predicate {
                    objects = objects.filter(predicate)
                }
                if let sortDescriptors = sortDescriptors, sortDescriptors.count > 0 {
                    objects = objects.sorted(by: sortDescriptors)
                }
               
                let unmanaged = Array(objects).map { $0.unmanaged() }
                DispatchQueue.main.async {
                    callback(unmanaged)
                }
            }
        }
    }
}

extension Object {
    
    func unmanaged() -> Self {
        if self.realm == nil {
            return self
        } else {
            return type(of: self).init(value: self, schema: .partialPrivateShared())
        }
    }
}
