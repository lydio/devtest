//
//  UsersViewController+TableView.swift
//  DevTest


import UIKit

fileprivate let rowHeight:CGFloat = 121

extension UsersViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return dataProvider.users.count
    }
    
    fileprivate typealias Cell = UserTableViewCell
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: Cell.identifier, for: indexPath) as! Cell
        let user = dataProvider.users[indexPath.row]
        cell.setUser(user)
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        self.presentInputViewController(withUserAtIndex: indexPath.row)
    }
}
