//
//  UsersViewController.swift
//  DevTest


import UIKit


class UsersViewController: UIViewController {

    private(set) var dataProvider:UsersDataProvider!
    
    var usersView:UsersView {
        return self.view as! UsersView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        firstTimeSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }

    func firstTimeSetup() {
        
        if let addUserButton = self.navigationItem.rightBarButtonItems?.first {
            addUserButton.target = self
            addUserButton.action = #selector(addNewUser(_:))
        }
        
        let tableView = self.usersView.tableView!
        tableView.dataSource = self
        tableView.delegate = self
        tableView.refreshControl?.addTarget(self, action: #selector(startRefreshAction(_:)), for: .valueChanged)
        
        dataProvider.reloadLocalData { [weak self] in
            
            if let strongSelf = self, !strongSelf.showExistedUsers() {
                strongSelf.startRefreshAction(nil)
            }
        }
        
        let noteName = AppAdditionalNotifications.didSendUserNotification
        NotificationCenter.default.addObserver(forName: noteName, object: nil, queue: nil) { [weak self] (note) in
            self?.startRefreshAction(nil)
        }
    }
    
    @objc func startRefreshAction(_ sender:Any?) {
        
        self.usersView.startRefreshControlIfNeeded()
        self.usersView.placeholderImageType = .isLoading
        
        dataProvider.reloadRemoteData(successCallback: { [weak self] in
            let _ = self?.showExistedUsers()
        }) { [weak self] (error) in
            self?.showPopup(error: error?.localizedDescription ?? "Unknown Error.")
        }
    }
    
    func showExistedUsers() -> Bool {
        
        let count = self.dataProvider.users.count
        self.usersView.refreshControl.endRefreshing()
        self.usersView.showPlaceholderOnBackground(count == 0)
        self.usersView.placeholderImageType = .notResultsFound
        self.usersView.tableView.reloadData()
        
        return count != 0
    }
    
    func showPopup(error:String) {
        
        self.usersView.refreshControl.endRefreshing()
        self.showPopup(title: "Error", message: error)
    }
    
    @objc func addNewUser(_ sender:Any?) {
        presentInputViewController(withUserAtIndex: nil)
    }
    
    func presentInputViewController(withUserAtIndex index:Int?) {
    
        let provider = self.dataProvider.inputDataProvider(atIndex: index ?? -1)
        present(type: UserInputViewController.self, dataProvider: provider, at: self, animated: true)
    }
}

extension UsersViewController:BaseViewController {
    
    static func viewController(with dataProvider: Any?) -> UIViewController {
        
        guard let dataProvider = dataProvider as? UsersDataProvider else {
            fatalError("UsersViewController: UsersDataProvider is required")
        }
        
        let storyboard = UIStoryboard(name: "Users", bundle: nil)
        let vc = storyboard.instantiateInitialViewController() as! UINavigationController
        (vc.viewControllers.first! as! UsersViewController).dataProvider = dataProvider
    
        return vc
    }
    
    func present(at primaryViewController: UIViewController, animated:Bool) {
        fatalError("UsersViewController: not implemented")
    }
}
