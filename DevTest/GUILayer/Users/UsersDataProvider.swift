//
//  UsersDataProvider.swift
//  DevTest


import UIKit

protocol UsersDataCoordinator {
    func refreshUsersList(callback: (([User], Swift.Error?)->())?)
    func usersList(callback: @escaping ([User])->())
}

class UsersDataProvider: NSObject {
    
    let coordinator:UsersDataCoordinator & UserInputDataCoordinator
    private(set) var users:[User]
    
    init(withCoordinator coordinator:UsersDataCoordinator & UserInputDataCoordinator) {
        
        self.coordinator = coordinator
        self.users = []
        
        super.init()
    }
    
    func reloadLocalData(callback:@escaping ()->()) {
        
        coordinator.usersList { [weak self] (users) in
            guard let strongSelf = self else {
                return
            }
            strongSelf.users = users
            callback()
        }
    }
    
    func reloadRemoteData(successCallback:@escaping ()->(), failCallback:((Swift.Error?)->())? = nil) {
        
        coordinator.refreshUsersList {  [weak self] (users, error) in
            
            guard error == nil else {
                failCallback?(error)
                return
            }
            self?.reloadLocalData(callback: successCallback)
        }
    }
    
    func inputDataProvider(atIndex index:Int) -> UserInputDataProvider {
        
        var user:User? = nil
        if index >= 0 && index < users.count {
            user = users[index]
        }
        let provider = UserInputDataProvider(withCoordinator: coordinator, user: user)
        return provider
    }
}
