//
//  UsersView.swift
//  DevTest


import UIKit
import SDWebImage


class UsersView: UIView {

    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var placeholderBackgroundView:UIImageView!
    let refreshControl = UIRefreshControl()
    
    private var initialSeparatorColor:UIColor?
    
    enum PlaceholderImageType:String {
        case notResultsFound, isLoading
    }
    var placeholderImageType:PlaceholderImageType = .notResultsFound {
        didSet {
            placeholderBackgroundView.image = UIImage(named: placeholderImageType.rawValue)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        tableView.register(UserTableViewCell.nib, forCellReuseIdentifier: UserTableViewCell.identifier)
        tableView.refreshControl = refreshControl
        
        initialSeparatorColor = tableView.separatorColor
        showPlaceholderOnBackground(false)
    }
    
    func showPlaceholderOnBackground(_ show:Bool) {
        
        placeholderBackgroundView.isHidden = !show
        tableView.separatorColor = show ? .clear : initialSeparatorColor
    }
    
    func startRefreshControlIfNeeded() {
        
        if !refreshControl.isRefreshing {
            refreshControl.beginRefreshing()
            
            let offset = CGPoint(x:0, y:tableView.contentOffset.y - refreshControl.bounds.size.height)
            if tableView.contentOffset.y > offset.y {
                tableView.setContentOffset(offset, animated: true)
            }
        }
    }
}


class UserTableViewCell: UITableViewCell, User {
    
    static let identifier = "UserTableViewCell"
    static let nib = UINib.init(nibName: "UserTableViewCell", bundle: nil)
    
    @IBOutlet weak var firstNameLabel:UILabel!
    @IBOutlet weak var lastNameLabel:UILabel!
    @IBOutlet weak var emailLabel:UILabel!
    @IBOutlet weak var webImageView:UIImageView!
    
    private let placeholderImage = UIImage(named:"placeholder.png")!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        webImageView.layer.cornerRadius = 12
        webImageView.image = placeholderImage
        lastNameLabel.text = ""
        firstNameLabel.text = ""
        emailLabel.text = ""
    }
    
    // User
    var first_name: String {
        get { return firstNameLabel.text ?? "" }
        set { firstNameLabel.text = newValue }
    }
    
    var last_name: String {
        get { return lastNameLabel.text ?? "" }
        set { lastNameLabel.text = newValue }
    }
    
    var email: String {
        get { return emailLabel.text ?? "" }
        set { emailLabel.text = newValue }
    }
    
    private var _image_url:String = ""
    var image_url: String {
        get {
           return _image_url
        }
        set {
            guard _image_url != newValue else {
                return
            }
            _image_url = newValue
            webImageView.sd_cancelCurrentImageLoad()

            guard let url = URL(string: newValue) else {
                webImageView.image = placeholderImage
                return
            }
            webImageView.sd_setImage(with: url, placeholderImage: placeholderImage)
        }
    }
    
    func setUser(_ user:User) {
        
        first_name = user.first_name
        last_name = user.last_name
        email = user.email
        image_url = user.image_url
    }
}


