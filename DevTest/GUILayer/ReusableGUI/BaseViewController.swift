//
//  BaseViewController.swift
//  DevTest


import UIKit

protocol BaseViewController where Self: UIViewController {

    static func viewController(with dataProvider: Any?) ->  UIViewController
    func present(at primaryViewController: UIViewController, animated:Bool)
}

extension UIViewController {
    
    func present<T>(type:T.Type, dataProvider:Any?, at primaryViewController: UIViewController, animated:Bool) where T: BaseViewController {
        
        let vc = T.viewController(with: dataProvider)
        (vc as? T)?.present(at: self, animated:animated)
    }
    
    func showPopup(title:String, message:String) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Close", style: .default))
        self.present(alertController, animated: true)
    }
}
