//
//  UserInputDataProvider.swift
//  DevTest


import UIKit

protocol UserInputDataCoordinator {
   func createUser(input:User, callback: @escaping (Bool, Swift.Error?)->())
   func editUser(input:User, id:String, callback: @escaping (Bool, Swift.Error?)->())
}

class UserInputDataProvider: NSObject {
    
    let coordinator: UserInputDataCoordinator
    private(set) var user:User?
    
    init(withCoordinator coordinator:UserInputDataCoordinator, user:User?) {
        
        self.coordinator = coordinator
        self.user = user
        
        super.init()
    }
    
    
    func sendUserInput(input: User, callback:((Swift.Error?)->())? = nil) {
        
        if var user = self.user as? (User & IdentifiedObject) {
            
            user.first_name = input.first_name
            user.last_name = input.last_name
            user.email = input.email
            user.image_url = input.image_url
            
            coordinator.editUser(input: user, id: user.id) { (success, error) in
                callback?(error)
            }
            
        } else {
            coordinator.createUser(input: input) { (success, error) in
                callback?(error)
            }
        }
        
    }
}
