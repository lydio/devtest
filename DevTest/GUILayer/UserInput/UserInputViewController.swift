//
//  UserInputViewController.swift
//  DevTest

import UIKit

let didSendUserNotification = Notification.Name(rawValue: "didSendUser")

class UserInputViewController: UIViewController {

    private(set) var dataProvider:UserInputDataProvider!
    
    var userInputView:UserInputView {
        return self.view as! UserInputView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        firstTimeSetup()
    }
    
    func firstTimeSetup() {
        
        if let user = self.dataProvider.user {
            userInputView.setUser(user)
        }
        
        let taps = userInputView.gestureRecognizers?.filter { $0 is UITapGestureRecognizer }
        taps?.first?.addTarget(userInputView, action: #selector(resignFirstResponder))
        
        userInputView.sendButton.addTarget(self, action: #selector(sendUser(_:)), for: .touchUpInside)
        
        for i in userInputView.inputs {
            i.textField.delegate = self
        }
    }
    
    @objc func sendUser(_ sender: Any?) {
        
        let _ = userInputView.resignFirstResponder()
        
        for i in userInputView.inputs {
            i.checkValidationRules()
            if i.hasErrors {
                self.showPopup(title: "Error", message: "Invalid Text in \"\(i.options.titleText)\"")
                return
            }
        }
        
        self.userInputView.showActivity = true
        
        self.dataProvider.sendUserInput(input: self.userInputView) { [weak self] (error) in
            
            guard let strongSelf = self else {
                return
            }
            strongSelf.userInputView.showActivity = false
            
            if let error = error {
                strongSelf.showPopup(title: "Error", message: error.localizedDescription)
            } else {
                
                let noteName = AppAdditionalNotifications.didSendUserNotification
                NotificationCenter.default.post(name: noteName, object: nil, userInfo: nil)
                
                strongSelf.navigationController?.popViewController(animated: true)
            }
        }
    }
}

extension UserInputViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
         self.userInputView.input(for: textField)?.clearValidationError()
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        self.userInputView.input(for: textField)?.checkValidationRules()
    }
    
}

extension UserInputViewController:BaseViewController {
    
    static func viewController(with dataProvider: Any?) -> UIViewController {
        
        guard let dataProvider = dataProvider as? UserInputDataProvider else {
            fatalError("UserInputViewController: UserInputDataProvider is required")
        }
        
        let storyboard = UIStoryboard(name: "UserInput", bundle: nil)
        let vc = storyboard.instantiateInitialViewController() as! UserInputViewController
        vc.dataProvider = dataProvider
        
        return vc
    }
    
    func present(at primaryViewController: UIViewController, animated:Bool) {

        guard let navigationController = primaryViewController.navigationController else {
            fatalError("UserInputViewController: navigation controller is required")
        }
        
        DispatchQueue.main.async {
            navigationController.pushViewController(self, animated: animated)
        }
    }
}
