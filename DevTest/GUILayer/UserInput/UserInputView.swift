//
//  UserInputView.swift
//  DevTest

import UIKit
import SkyFloatingLabelTextField
import SwiftValidator


fileprivate let inputSettings: [(title:String, placeholder:String, rules:[Rule])] = [
    (
        "First Name",
        "Enter First Name",
        [RequiredRule(message: "First Name is Required"), AlphaRule(message: "Invalid Characters")]
    ),
    (
        "Last Name",
        "Enter Last Name",
        [RequiredRule(message: "Last Name is Required"), AlphaRule(message: "Invalid Characters")]
    ),
    (
        "Email",
        "Enter Your Email",
        [RequiredRule(message: "Email is Required"), EmailRule(message: "Invalid Format")]
    ),
    (
        "Image Link",
        "Enter HTTP Ref",
        [RegexRule(regex: "(http|https)://((\\w)*|([0-9]*)|([-|_])*)+([\\.|/]((\\w)*|([0-9]*)|([-|_])*))+", message: "Invalid Format")]
    ),
]

class UserInputView: UIView {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet var inputFieldSuperviews: [UIView]!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var activityView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    private(set) var inputs = [InputItem]()
    
    var showActivity:Bool = false {
        didSet{
            let show = showActivity
            DispatchQueue.main.asyncAfter(deadline: .now() + (show ? 0 : 1)) { [weak self] in
                guard let strongSelf = self else {
                    return
                }
                strongSelf.activityView.isHidden = !show
                if show {
                    strongSelf.activityIndicator.startAnimating()
                } else {
                    strongSelf.activityIndicator.stopAnimating()
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        activityView.isHidden = true
        activityView.isUserInteractionEnabled = false
        
        if inputFieldSuperviews.count < inputSettings.count {
            fatalError("UserInputView: out of bounds")
        }
        
        for i in 0..<inputSettings.count {
            
            let s = inputSettings[i]
            let v = inputFieldSuperviews[i]
            var o = InputItem.InitialOptions.defaultOptions()
            
            o.titleText = s.title
            o.placeholderText = s.placeholder
            o.validationRules = s.rules
            
            inputs.append(InputItem(with: o, inContainer: v))
        }
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        inputs.forEach { $0.updateLayout() }
    }
    
    override func resignFirstResponder() -> Bool {
    
        inputs.forEach { let _ =
            $0.textField.resignFirstResponder()
        }
        return  super.resignFirstResponder()
    }
    
    func input(for textField:UITextField) -> InputItem? {
        for i in inputs {
            if i.textField == textField {
                return i
            }
        }
        return nil
    }
    
    class InputItem: NSObject {
        
        struct InitialOptions {
            var width:CGFloat
            var height: CGFloat
            var alignment: UIViewContentMode
            var darkColor: UIColor
            var lightColor: UIColor
            var errorColor: UIColor
            var validationRules:[Rule]
            var placeholderText:String
            var titleText:String
            
            static func defaultOptions() -> InitialOptions {
                return InitialOptions(
                    width: 300,
                    height: 42,
                    alignment: .center,
                    darkColor: .black,
                    lightColor: .gray,
                    errorColor: .red,
                    validationRules: [],
                    placeholderText: "",
                    titleText: ""
                )
            }
        }
        
        let options:InitialOptions
        private weak var container:UIView?

        private static var _validator:Validator? = nil
        var validator:Validator {
            return type(of: self)._validator!
        }
        
        let textField:SkyFloatingLabelTextField
        private(set) var hasErrors:Bool = false
        
        deinit {
            self.validator.unregisterField(textField)
        }
        
        init(with options:InitialOptions, inContainer container:UIView?) {
            
            self.options = options
            self.container = container
            
            if type(of: self)._validator == nil {
                type(of: self)._validator = Validator()
            }
            
            let rect = CGRect(x: 0, y: 0, width: options.width, height: options.height)
            textField = SkyFloatingLabelTextField(frame: rect)
            
            super.init()
            
            container?.addSubview(textField)
            updateLayout()
            
            self.validator.registerField(textField, rules: options.validationRules)
            
            textField.title = options.titleText
            textField.placeholder = options.placeholderText
            textField.lineColor = options.darkColor
            textField.textColor = options.darkColor
            textField.selectedTitleColor = options.darkColor
            textField.titleColor = options.darkColor
            textField.errorColor = options.errorColor
            
            textField.autocorrectionType = .no
            textField.autocapitalizationType = .none
            textField.spellCheckingType = .no
        }
        
        func updateLayout() {
            
            guard let container = container else {
                return
            }
            
            let size = container.bounds.size
            
            switch options.alignment {
                case .center:
                    textField.center = CGPoint(x: size.width/2, y: size.height/2)
                    break
                default:
                    fatalError("InputItem: not implemented")
            }
        }
        
        func checkValidationRules() {
            
            validator.validateField(textField) { [weak self] (error) in
                
                let failed = (error != nil)
                textField.errorMessage = failed ? error?.errorMessage : ""
                if let strongSelf = self {
                    strongSelf.hasErrors = failed
                }
            }
        }
        
        func clearValidationError() {
            hasErrors = false
            textField.errorMessage = ""
        }
    }
}

extension UserInputView: User {
    
    var first_name: String {
        get { return inputs[0].textField.text ?? "" }
        set { inputs[0].textField.text = newValue }
    }
    
    var last_name: String {
        get { return inputs[1].textField.text ?? "" }
        set { inputs[1].textField.text = newValue }
    }
    
    var email: String {
        get { return inputs[2].textField.text ?? "" }
        set { inputs[2].textField.text = newValue }
    }
    
    var image_url: String {
        get { return inputs[3].textField.text ?? "" }
        set { inputs[3].textField.text = newValue }
    }
    
    func setUser(_ user:User) {
        
        first_name = user.first_name
        last_name = user.last_name
        email = user.email
        image_url = user.image_url
    }
}
