//
//  AppDelegate.swift
//  DevTest


import UIKit
import RealmSwift
import IQKeyboardManagerSwift
import SDWebImage


struct AppAdditionalNotifications {
    static let didSendUserNotification = Notification.Name(rawValue: "didSendUser")
}


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    let dataCoordinator = DataCoordinator()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        let w = UIWindow(frame: UIScreen.main.bounds)
        let dataProvider = UsersDataProvider(withCoordinator: self.dataCoordinator)
        w.rootViewController = UsersViewController.viewController(with: dataProvider)
        self.window = w
        w.makeKeyAndVisible()
        
        IQKeyboardManager.shared.enable = true
        SDImageCache.shared().config.maxCacheSize = 16 * 1024 * 1024
        
        print(Realm.Configuration.defaultConfiguration.fileURL ?? "")
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        
    }


}

