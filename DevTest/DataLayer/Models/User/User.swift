//
//  User.swift
//  DevTest


import Foundation

protocol User {
    
    var first_name:String { get set }
    var last_name:String { get set }
    var email:String { get set }
    var image_url:String { get set }
}
