//
//  UserRealm.swift
//  DevTest


import Foundation
import RealmSwift


final class UserRealm: Object, IdentifiedObject, User, Decodable {
    
    @objc dynamic var id:String = ""
    @objc dynamic var first_name:String = ""
    @objc dynamic var last_name:String = ""
    @objc dynamic var email:String = ""
    @objc dynamic var image_url:String = ""
    @objc dynamic var created:String = ""
    @objc dynamic var updated:String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
    convenience init(with fields:User) {
        self.init()
        
        self.first_name = fields.first_name
        self.last_name = fields.last_name
        self.email = fields.email
        self.image_url = fields.image_url
    }
}

extension UserRealm:Encodable {
    
    enum ObjectKey: String, CodingKey {
        case user
    }
    
    enum FieldKeys: String, CodingKey {
        case first_name
        case last_name
        case email
        case image_url
    }
    
    func encode(to encoder: Encoder) throws {
        
        var object = encoder.container(keyedBy: ObjectKey.self)
        var fields = object.nestedContainer(keyedBy: FieldKeys.self, forKey: .user)
        
        try fields.encode(first_name, forKey: .first_name)
        try fields.encode(last_name, forKey: .last_name)
        try fields.encode(email, forKey: .email)
    }
}
