//
//  DataCoordinator.swift
//  DevTest


import Foundation


class DataCoordinator:NSObject, UsersDataCoordinator, UserInputDataCoordinator {
    
    let network = Network()
    let database = Database()
    
    func refreshUsersList(callback: (([User], Swift.Error?)->())?) {
        
        let endpoint = ServerAPI.get(object: (type: UserRealm.self, id: nil))
        
        network.defaultRequest(endpoint: endpoint) { [weak self] (users:[UserRealm]?, error:Swift.Error?) in
            
            guard let strongSelf = self else {
                return
            }
            guard error == nil else {
                callback?([], error)
                return
            }
        
            strongSelf.database.replaceAll(by: users ?? [], callback: { [weak self] in
                
                if let strongSelf = self, let callback = callback {
                    strongSelf.database.fetchAll(UserRealm.self, filter: nil, sort: nil, callback: { (users) in
                        callback(users, nil)
                    })
                }
            })
        }
    }
    
    func usersList(callback: @escaping ([User])->()) {
        
        database.fetchAll(UserRealm.self, filter: nil, sort: nil, callback: callback)
    }
    
    func createUser(input:User, callback: @escaping (Bool, Swift.Error?)->()) {
        
        let userRealm = UserRealm(with: input)
        let endpoint = ServerAPI.create(object: userRealm)
        
        network.defaultRequest(endpoint: endpoint) { (_:UserRealm?, error:Swift.Error?) in
            callback(error == nil, error)
        }
    }
    
    func editUser(input:User, id:String, callback: @escaping (Bool, Swift.Error?)->()) {
        
        let userRealm = UserRealm(with: input)
        userRealm.id = id
        let endpoint = ServerAPI.edit(object: userRealm)
        
        network.defaultRequest(endpoint: endpoint) { (_:UserRealm?, error:Swift.Error?) in
            callback(error == nil, error)
        }
    }
}
