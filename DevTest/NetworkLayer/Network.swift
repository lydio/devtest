//
//  NetworkManager.swift


import Foundation
import Moya
import Result


class Network:NSObject {
    
    private let provider = MoyaProvider<ServerAPI>(plugins: [AuthTokenPlugin(), NetworkLoggerPlugin(verbose: true)])
    private let reachability = Reachability.forInternetConnection()!
    private var cancelable = [Int:Cancellable]()
    
    var reachable:Bool {
        return reachability.currentReachabilityStatus() != NotReachable
    }
    
    private var nextCancelKey:Int {
        return (cancelable.keys.max() ?? 0) + 1
    }
    
    override init() {
        super.init()
        
        reachability.startNotifier()
    }
    
    func cancelAllRequests() {
        
        guard Thread.isMainThread else {
            fatalError("### Network: main thread required")
        }
        cancelable.values.forEach { $0.cancel() }
        cancelable.removeAll()
    }
    
    func defaultRequest<T>(endpoint:ServerAPI, completion: @escaping (T?, Swift.Error?)->()) where T:Decodable {
        
        guard Thread.isMainThread else {
            fatalError("### Network: main thread required")
        }
        guard reachable else {
            let error = NSError(domain: NSURLErrorDomain, code: NSURLErrorNotConnectedToInternet, userInfo: nil)
            completion(nil, error)
            return
        }
        
        let key = nextCancelKey
        cancelable[key] = provider.request(endpoint) { [weak self] (result) in
            
            var decoded:T? = nil
            var error:Swift.Error? = nil
            
            switch result {
                case let .success(response):
                    do {
                        if response.data.count > 0 {
                            decoded = try JSONDecoder().decode(T.self, from: response.data)
                        }
                    } catch let decodingError {
                        error = decodingError
                    }
                case let .failure(networkingError):
                    error = networkingError
            }
            
            DispatchQueue.main.async { [weak self] in
                
                guard let value = self?.cancelable[key], !value.isCancelled else {
                    return
                }
                self?.cancelable.removeValue(forKey: key)
                completion(decoded, error)
            }
        }
    }
}




