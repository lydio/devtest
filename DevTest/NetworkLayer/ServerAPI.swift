//
//  ServerAPI.swift
//  DevTest

import Foundation
import Moya


protocol IdentifiedObject {
    var id:String { get set }
}

enum ServerAPI {
    
    typealias Object = AnyObject & IdentifiedObject & Encodable
    
    case get(object:(type:Object.Type, id:String?))
    case create(object:Object)
    case edit(object:Object)
    case delete(object:(type:Object.Type, id:String))
    
    static let basePath =  "https://cua-users.herokuapp.com/"
    
    static func path(for type:Object.Type, method: Moya.Method, id:String?) -> String {
        
        var path:String
       
        switch type {
            case is User.Type:
                path = method == .put ? "edit_user.php" : "users.php"
                break
            default:
                fatalError("### ServerAPI: path not found")
        }
        
        guard let id = id, id.count > 0 else {
            return path
        }
        
        switch type {
            case is User.Type:
                path += "?user_id=\(id)"
                break
            default:
                break
        }
        
        return path
    }
}


extension ServerAPI: TargetType, AccessTokenAuthorizable {
    
    var baseURL: URL {
        
        let api = type(of: self)
        let base = api.basePath
        var components = ""
        
        switch self {
            case .get(let object):
                components = api.path(for: object.type, method: self.method, id: object.id)
            case .create(let object):
                components = api.path(for: type(of: object), method: self.method, id: object.id)
            case .edit(let object):
                components = api.path(for: type(of: object), method: Moya.Method.put, id: object.id)
            case .delete(let object):
                components = api.path(for: object.type, method: self.method, id: object.id)
        }
        
        return URL(string: base + components)!
    }
    
    var path: String {
        // unusable, fixed in baseURL
        return ""
    }
   
    var method: Moya.Method {
        
        switch self {
            case .create:
                return .post
            case .edit:
                return .post
            case .delete:
                return .delete
            default:
                return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        
        switch self {
            case .create(let object):
                return .requestJSONEncodable(object)
            case .edit(let object):
                return .requestJSONEncodable(object)
            default:
                break
        }
        return .requestParameters(parameters: [:], encoding: URLEncoding.httpBody)
    }
    
    var headers: [String : String]? {
        return ["Content-type": "application/json", "Accept": "application/json"]
    }
    
    var authorizationType: AuthorizationType {
        switch self {
            default:
                return .none
        }
    }
}

public struct AuthTokenPlugin: PluginType {
    
    public func prepare(_ request: URLRequest, target: TargetType) -> URLRequest {
        // no authorization
        return request
    }
}


